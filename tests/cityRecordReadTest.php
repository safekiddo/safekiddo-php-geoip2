<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use SafeKiddo\GeoIp2\ReaderFactory;
use SafeKiddo\GeoIp2\Exception\ReaderNotInitialisedException;

$reader = ReaderFactory::Instance();

try {
    $reader->configure('/usr/share/GeoIP/GeoLite2-City.mmdb');
    $reader->getReader();
    
    $city = $reader->getCity('128.101.101.101'); 
    assert($city->country->isoCode == 'US');
    assert($city->location->timeZone == 'America/Chicago');   
    
    $city = $reader->getCity('223.29.192.183');
    assert($city->country->isoCode == 'IN');  
    assert($city->location->timeZone == 'Asia/Kolkata');
    

    
    
} catch (\Exception $ex) {
    throw $ex;
}
