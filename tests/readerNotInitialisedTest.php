<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload

use SafeKiddo\GeoIp2\ReaderFactory;
use SafeKiddo\GeoIp2\Exception\ReaderNotInitialisedException;

$reader = ReaderFactory::Instance();

try {
    $reader->getReader();
    
} catch (ReaderNotInitialisedException $ex) {

} catch (\Exception $ex) {
    throw $ex;
}
