<?php

/*
 * Copyright by SafeKiddo
 * MIT License
 */

namespace SafeKiddo\GeoIp2\Exception;
/**
 * Thrown when reader is not initialized before calling getReader()
 *
 * @author mmarzec
 */
class ReaderNotInitialisedException extends \RuntimeException {
    //put your code here
}
