<?php

namespace SafeKiddo\GeoIp2;

use GeoIp2\Database\Reader;
use SafeKiddo\GeoIp2\Exception\ReaderNotInitialisedException;
use SafeKiddo\GeoIp2\Exception\ReaderAlreadyInitialisedException;
use GeoIp2\Model\City;
/**
 * GeoIp2 Reader singleton class
 *
 */
final class ReaderFactory
{
    
    /**
     * @var Reader
     */    
    static protected $reader;
    
    /**
     * @var boolean
     */    
    static protected $configured;
    
        

    /**
     * Private constructor so nobody else can instance it
     *
     */
    private function __construct()
    {
        self::$configured = false;
    }
    
    /**
     * Call this method to get singleton
     *
     * @return ReaderFactory
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new ReaderFactory();
        }
        return $inst;
    }

    /**
     * Call this method to set path to database file
     *
     * @param string path
     */
    public static function configure($filepath)
    {
        if (is_null(self::$reader) and (!self::$configured)) {
            self::$reader = new Reader($filepath);
            self::$configured = true;
        } else {
            throw new ReaderAlreadyInitialisedExceptio();
        }
    }

    /**
     * Call this method to get reader
     *
     * @param string path
     */
    public static function getReader()
    {
        if (!is_null(self::$reader)) {
            return self::$reader;
        } else {
            throw new ReaderNotInitialisedException();
        }
    }    

    /**
     * Get configuration status
     * @return string path
     */
    public static function isConfigured()
    {
        return self::$configured;
    } 
    
    /**
     * Get City record for IP Address
     *
     * @param string $ip
     * @return City
     */
    public static function getCity($ip)
    {
        if (is_null(self::$reader)) {
            throw new ReaderNotInitialisedException();
        }
        
        $cityRecord = self::$reader->city($ip);
        return $cityRecord;
    }
    
    
}